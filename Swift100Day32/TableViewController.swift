//
//  TableViewController.swift
//  Swift100Day32
//
//  Created by Roman Brazhnikov on 21/04/2019.
//  Copyright © 2019 Roman Brazhnikov. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    var goodsToBuy: [String] = ["One", "Two"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(askForClearAll))
        
        let addRightButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(askForItem))
        let shareRightButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareList))
        navigationItem.rightBarButtonItems = [addRightButton, shareRightButton]
    }

    @objc func shareList() {
        let listAsString = goodsToBuy.joined(separator: "\n")
        let vc = UIActivityViewController(activityItems: [listAsString], applicationActivities: nil)
        
        vc.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        present(vc, animated: true)
    }
    
    @objc func askForClearAll(){
        
        let ac = UIAlertController(title: "Remove all items?", message: nil, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let removeAction = UIAlertAction(title: "Remove All", style: .destructive) {
            action in
            
            self.clearAll()
        }
        
        ac.addAction(cancelAction)
        ac.addAction(removeAction)
        
        present(ac, animated: true)
    }
    
    func clearAll(){
        goodsToBuy.removeAll()
        tableView.reloadData()
    }
    
    @objc func askForItem() {
        let ac = UIAlertController(title: "Add item", message: "What do you want to buy?", preferredStyle: .alert)
        
        ac.addTextField()
        
        let addAction = UIAlertAction(title: "Add", style: .default){
            [weak ac] action in
            guard let text = ac?.textFields?[0].text else {
                return
            }
            self.addItemToGoodsList(item: text)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        ac.addAction(cancelAction)
        ac.addAction(addAction)
        
        present(ac, animated: true)
    }
    
    func addItemToGoodsList(item: String){
        goodsToBuy.insert(item, at: 0)
        let indexPath = IndexPath(item: 0, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return goodsToBuy.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainCell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = goodsToBuy[indexPath.row]

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
